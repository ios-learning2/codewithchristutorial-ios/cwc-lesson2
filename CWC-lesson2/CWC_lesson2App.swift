//
//  CWC_lesson2App.swift
//  CWC-lesson2
//
//  Created by Maciek Janik on 28/12/2023.
//

import SwiftUI

@main
struct CWC_lesson2App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
