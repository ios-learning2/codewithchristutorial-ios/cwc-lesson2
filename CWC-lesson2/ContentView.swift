//
//  ContentView.swift
//  CWC-lesson2
//
//  Created by Maciek Janik on 28/12/2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "person")
                .imageScale(.large)
                .foregroundStyle(.tint)
            Text("Lesson2!")
        }
        .padding()
    }
}

#Preview {
    ContentView()
}
